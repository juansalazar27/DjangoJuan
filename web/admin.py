from django.contrib import admin
from .models import Banners, CatBanners, Custom, CatCustom


class AdminBanners(admin.ModelAdmin):
    list_display = ["__str__", "title", "description", "imagen"]
    class Meta:
        model = Banners

class AdminCustom(admin.ModelAdmin):
    list_display = ["__str__", "title", "content", "imagen"]
    class Meta:
        model = Custom

admin.site.register(Banners, AdminBanners)
admin.site.register(CatBanners)
admin.site.register(Custom, AdminCustom)
admin.site.register(CatCustom)
