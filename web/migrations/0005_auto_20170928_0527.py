# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-09-28 05:27
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0004_custom_categoria'),
    ]

    operations = [
        migrations.RenameField(
            model_name='catbanners',
            old_name='name',
            new_name='title',
        ),
        migrations.RenameField(
            model_name='catcustom',
            old_name='name',
            new_name='title',
        ),
    ]
