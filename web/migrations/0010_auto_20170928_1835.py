# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-09-28 18:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0009_auto_20170928_1834'),
    ]

    operations = [
        migrations.AlterField(
            model_name='custom',
            name='content',
            field=models.TextField(),
        ),
    ]
