from __future__ import unicode_literals
from django.db import models

# Create your models here.

class CatBanners(models.Model):
    title = models.CharField(max_length=30, blank=True, null=True)

    def __str__(self):
        return self.title

class Banners(models.Model):
    categoria = models.ForeignKey(CatBanners, on_delete=models.CASCADE)
    title = models.CharField(max_length=30, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    imagen = models.FileField(upload_to='banners', null=True, blank=True)

    def __str__(self):
        return self.title

class CatCustom(models.Model):
    title = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.title

class Custom(models.Model):
    categoria = models.ForeignKey(CatCustom, on_delete=models.CASCADE)
    title = models.CharField(max_length=255, null=True, blank=True)
    content = models.TextField(null=True, blank=True)
    imagen = models.FileField(upload_to='customs', null=True, blank=True)


    def __str__(self):
        return self.title
