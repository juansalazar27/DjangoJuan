from django import template
register = template.Library()

from web.models import *

@register.simple_tag
def banners(cate):
    custom = CatBanners.objects.filter(title=cate).first()
    return Banners.objects.filter(categoria = custom)

@register.simple_tag
def customs(cust):
    custom = CatCustom.objects.filter(title=cust).first()
    return Custom.objects.filter(categoria = custom)

@register.filter(name='times')
def times(number):
    return range(number)
