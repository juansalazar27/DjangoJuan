from django.conf.urls import url
from django.contrib import admin
from django.views.generic import TemplateView

urlpatterns= [
    url(r'^$', TemplateView.as_view(template_name='index.html'), name='index'),
    url(r'^nosotros/', TemplateView.as_view(template_name='nosotros.html'), name='nosotros'),
]
